import pyaudio
import os
import struct
import numpy as np

from scipy.fftpack import fft 

import pygame.midi 
pygame.midi.init()
import math
# use this backend to display in separate Tk window

# constants
CHUNK = 1024 * 8           # samples per frame. I think this should be the amount of samples to cover the time it takes for 2 full cycles of the LOWEST frequency
FORMAT = pyaudio.paInt16     # audio format (bits per sample)
CHANNELS = 1                 # single channel for microphone
RATE = 48000              # samples per second, should be AT LEAST twice the desired highest frequency, higher is better.
DEVICEINDEX = 10

# pyaudio class instance
p = pyaudio.PyAudio()

# stream object to get data from microphone
stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    output=True,
    frames_per_buffer=CHUNK,
    input_device_index = DEVICEINDEX
)

# variable for plotting
#x = np.arange(0, 2 * CHUNK, 2)
x_fft = np.linspace(0, RATE, CHUNK)


while True:
    y_fft_combined = []
    for i in range(1):
        data = stream.read(CHUNK, exception_on_overflow = False)  
        data_int = struct.unpack(str(2 * CHUNK) + 'B', data)
        y_fft_combined.append(fft(data_int))

    y_fft_average = tuple(map(lambda y: sum(y) / float(len(y)), zip(*y_fft_combined)))
    absAverage = np.abs(y_fft_average[0:CHUNK]) * 3

    freq = np.argmax(absAverage[1:1024]) * RATE/CHUNK + 14
    if freq < 1000:
        note = pygame.midi.frequency_to_midi(freq) 
        print(str(math.floor(freq)) + " - " + str(note) + " - " + pygame.midi.midi_to_ansi_note(note))
