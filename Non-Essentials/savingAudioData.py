import pyaudio
import os
import struct
import numpy as np
import pickle

from scipy.fftpack import fft 

import pygame.midi 
pygame.midi.init()
import math
# use this backend to display in separate Tk window

# constants
CHUNK = 1024          # samples per frame. I think this should be the amount of samples to cover the time it takes for 2 full cycles of the LOWEST frequency
FORMAT = pyaudio.paInt16     # audio format (bits per sample)
CHANNELS = 1                 # single channel for microphone
RATE = 48000              # samples per second, should be AT LEAST twice the desired highest frequency, higher is better.
DEVICEINDEX = 10

# pyaudio class instance
p = pyaudio.PyAudio()

# stream object to get data from microphone
stream = p.open(
    format=FORMAT,
    channels=CHANNELS,
    rate=RATE,
    input=True,
    output=True,
    frames_per_buffer=CHUNK,
    input_device_index = DEVICEINDEX
)

# variable for plotting
#x = np.arange(0, 2 * CHUNK, 2)

input()
dataList = []
for i in range(32):
    dataList.append(stream.read(CHUNK, exception_on_overflow = False))
    print(len(dataList))

filename = "49Hz"
open(filename, "a").close
f = open(filename, "wb")
pickle.dump(dataList, f)
f.close()
print("Data printed")

