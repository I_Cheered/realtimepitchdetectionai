from resources.MiscResources import *

class HighLighter:
    def __init__(self, screen, eventHandler):
        self.eventHandler = eventHandler
        self.screen = screen 
        self.screensize = [self.screen.get_size()[0], self.screen.get_size()[1]]
        self.x = 0
        self.y = 0
        self.width = 0
        self.height = 0
        self.text = [""]
        self.whatButton = Button(self.screen, "What?", getFontSizeBasedOnSize("What?", self.screensize[1] * 1 / 13, "height"), self.screensize[0] * 9/10, self.screensize[1] / 10)

    def highlight(self, x, y, width, height, text, pos, orientation): #this function updates location of what to highlight
        padding = self.screensize[1] / 20
        self.pos = pos
        self.x = x - padding/2
        if self.x < 0:
            self.x = 0
        self.y = y  - padding/2
        if self.y < 0:
            self.y = 0
        self.width = width + padding
        self.height = height + padding
        if self.width > self.screensize[0]:
            self.width = self.screensize[0]
        if self.height > self.screensize[1]:
            self.height = self.screensize[1]
        
        if self.x + self.width > self.screensize[0]:
            self.width = self.screensize[0] - self.x
        if self.y + self.height > self.screensize[1]:
            self.height = self.screensize[1] - self.y
        self.text = text 
        self.orientation = orientation

        self.drawOpacity()            
        self.drawText()
        return self.drawButton()

    def drawButton(self):
        text = "Okay"
        if self.orientation == "under":
            self.button = Button(self.screen, text, getFontSizeBasedOnSize(text, self.screensize[1] * 1 / 13, "height"), (self.x + (self.width / 2)), self.y + self.height + self.screensize[1] * 2 / 20 + self.screensize[1] * 1 / 18 * len(self.text))
        if self.orientation == "right":
            self.button = Button(self.screen, text, getFontSizeBasedOnSize(text, self.screensize[1] * 1 / 13, "height"), self.x + self.width + 100, self.y + self.height / 2 + self.screensize[1] * 2 / 20 + self.screensize[1] * 1 / 18 * len(self.text))
        if self.orientation == "center":
            self.button = Button(self.screen, text, getFontSizeBasedOnSize(text, self.screensize[1] * 1 / 13, "height"), self.screensize[0] / 2, self.screensize[1] / 2 + self.screensize[1] * 1 / 20 + len(self.text) * self.screensize[1] * 1 / 18)
        if self.orientation == "above":
            self.button = Button(self.screen, text, getFontSizeBasedOnSize(text, self.screensize[1] * 1 / 13, "height"), (self.x + (self.width / 2)), self.y - self.screensize[1] * 2 / 20 - self.screensize[1] * 1 / 18 * len(self.text))
        self.button.hoverOn(self.pos)
        self.button.draw()
        if pygame.mouse.get_pressed()[0] == True and self.eventHandler.mouseWasPressed == False:
            self.eventHandler.mouseWasPressed = True
            if self.button.onClick() == True:
                return 1
            else:
                return 0
        else:
            return 0

    def drawText(self): #this function draws the text during the explanation
        text = self.text[0]
        fontsize = getFontSizeBasedOnSize(text, self.screensize[1] * 1 / 18, "height")
        for i in range(len(self.text)):
            text = self.text[i]
            if self.orientation == "under":
                x = self.x + (self.width / 2)
                y = self.y + self.height + self.screensize[1] * 1 / 20 + self.screensize[1] * 1 / 18 * i
            if self.orientation == "right":
                x = self.x + self.width * 1.5
                y = self.y + self.height / 2 - self.screensize[1] * 3 / 20 + self.screensize[1] * 1 / 18 * i
            if self.orientation == "center":
                x = self.screensize[0] / 2
                y = self.screensize[1] / 2 + self.screensize[1] * 1 / 18 * i
            if self.orientation == "above":
                x = self.x + (self.width / 2)
                y = self.y - self.screensize[1]*1/18*len(self.text) + self.screensize[1] * 1 / 17 * i
            textObject = Text(self.screen, text, fontsize, x, y)
            textObject.draw()

    def drawOpacity(self): #this function draws the highlighted area (lower opacity everywhere else and draw outline)
        top = pygame.Surface((self.screensize[0], self.y))
        bot = pygame.Surface((self.screensize[0], self.screensize[1] - (self.y + self.height)))
        left = pygame.Surface((self.x, self.height))
        right = pygame.Surface((self.screensize[0] - (self.x + self.width), self.height))
        outline = pygame.draw.rect(self.screen, (255, 255, 255), [self.x, self.y, self.width, self.height], 3)
        
        alphaValue = 200 
        top.set_alpha(alphaValue)
        bot.set_alpha(alphaValue)
        left.set_alpha(alphaValue)
        right.set_alpha(alphaValue)

        fillColor = (0, 0, 0)
        top.fill(fillColor)
        bot.fill(fillColor)
        left.fill(fillColor)
        right.fill(fillColor)

        self.screen.blit(top, (0, 0))
        self.screen.blit(bot, (0, self.y + self.height))
        self.screen.blit(left, (0, self.y))
        self.screen.blit(right, ((self.x + self.width), self.y))