from resources.BaseUIClass import *

class MainUI(UI):
    def __init__(self, screen):
        super().__init__(screen)
        self.titleText = "PiaNotes"
        self.titleFontSize = getFontSizeBasedOnSize(self.titleText, self.height * 1.5 / 5, "height")
        self.titleXpos = self.width / 2
        self.titleYpos = self.height / 5

        self.inputMethodText = "Choose an input method"
        self.inputMethodFontSize = getFontSizeBasedOnSize(self.inputMethodText, self.height * 1 / 13, "height")
        self.inputMethodXpos = self.width / 2
        self.inputMethodYpos = self.height * 3.5 / 10
        
        self.enharmonicsText = "Choose enharmonics (Sharps/Flats)"
        self.enharmonicsFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.enharmonicsXpos = self.width / 2
        self.enharmonicsYpos = self.height * 6 / 10

        self.buttonText = "Start"
        self.buttonFontSize = getFontSizeBasedOnSize(self.enharmonicsText, self.height * 1 / 13, "height")
        self.buttonXpos = self.width/2
        self.buttonYpos = self.height * 9 / 10
        
        self.keyboardText = "Keyboard"
        self.keyboardFontSize = getFontSizeBasedOnSize(self.keyboardText, self.height * 1 / 18, "height")
        self.keyboardXpos = self.width/2 - self.width / 8
        self.keyboardYpos = self.height*4.5/10

        self.audioText = "Audio"
        self.audioFontSize = getFontSizeBasedOnSize(self.audioText, self.height * 1 / 18, "height")
        self.audioXpos = self.width/2
        self.audioYpos = self.height*4.5/10

        self.pianoText = "Piano (MIDI)"
        self.pianoFontSize = getFontSizeBasedOnSize(self.pianoText, self.height * 1 / 18, "height")
        self.pianoXpos = self.width/2 + self.width / 8
        self.pianoYpos = self.height*4.5/10

        self.vanillaText = "Vanilla"
        self.vanillaFontSize = getFontSizeBasedOnSize(self.vanillaText, self.height * 1 / 18, "height")
        self.vanillaXpos = self.width/2 - self.width / 10
        self.vanillaYpos = self.height*7/10

        self.sharpsText = "Sharps"
        self.sharpsFontSize = getFontSizeBasedOnSize(self.sharpsText, self.height * 1 / 18, "height")
        self.sharpsXpos = self.width/2
        self.sharpsYpos = self.height*7/10

        self.flatsText = "Flats"
        self.flatsFontSize = getFontSizeBasedOnSize(self.flatsText, self.height * 1 / 18, "height")
        self.flatsXpos = self.width/2 + self.width / 10
        self.flatsYpos = self.height*7/10

        self.title = Text(self.screen, self.titleText, self.titleFontSize, self.titleXpos, self.titleYpos)
        self.inputMethodText = Text(self.screen, self.inputMethodText, self.inputMethodFontSize, self.inputMethodXpos, self.inputMethodYpos)
        self.enharmonicsText = Text(self.screen, self.enharmonicsText, self.enharmonicsFontSize, self.enharmonicsXpos, self.enharmonicsYpos)
        self.keyboardButton = Button(self.screen, self.keyboardText, self.keyboardFontSize, self.keyboardXpos, self.keyboardYpos)
        self.pianoButton = Button(self.screen, self.pianoText, self.pianoFontSize, self.pianoXpos, self.pianoYpos)
        self.audioButton = Button(self.screen, self.audioText, self.audioFontSize, self.audioXpos, self.audioYpos)
        self.vanillaButton = Button(self.screen, self.vanillaText, self.vanillaFontSize, self.vanillaXpos, self.vanillaYpos)
        self.sharpsButton = Button(self.screen, self.sharpsText, self.sharpsFontSize, self.sharpsXpos, self.sharpsYpos)
        self.flatsButton = Button(self.screen ,self.flatsText, self.flatsFontSize, self.flatsXpos, self.flatsYpos)
        self.startButton = Button(self.screen, self.buttonText, self.buttonFontSize, self.buttonXpos, self.buttonYpos)
        self.keyboardButton.isSelected = True
        self.vanillaButton.isSelected = True

    def draw(self):
        self.title.draw()
        self.inputMethodText.draw()
        self.enharmonicsText.draw()
        self.keyboardButton.draw()
        self.pianoButton.draw()
        self.vanillaButton.draw()
        self.sharpsButton.draw()
        self.flatsButton.draw()
        self.startButton.draw()
        self.highLighter.whatButton.draw()
        self.audioButton.draw()
    
    def isActive(self):
        self.eventHandler.inGame = False
        self.eventHandler.inLevelSelect = False
        self.eventHandler.inMainMenu = True
        pos = pygame.mouse.get_pos()
        if self.eventHandler.inWhatButton == False:
            self.startButton.hoverOn(pos)
            self.keyboardButton.hoverOn(pos)
            self.pianoButton.hoverOn(pos)
            self.audioButton.hoverOn(pos)
            self.vanillaButton.hoverOn(pos)
            self.sharpsButton.hoverOn(pos)
            self.flatsButton.hoverOn(pos)
            self.highLighter.whatButton.hoverOn(pos)

            if pygame.mouse.get_pressed()[0] == True and self.eventHandler.mouseWasPressed == False:
                self.eventHandler.mouseWasPressed = True
                if self.highLighter.whatButton.onClick() == True:
                    self.eventHandler.inWhatButton = True        
                if self.startButton.onClick() == 1:
                    try:
                        self.eventHandler.initiateInputMethod()
                        self.eventHandler.gameState += 1
                    except:
                        print("Error, could not initiate input device")
                if self.pianoButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Piano"
                    self.pianoButton.isSelected = True
                    self.keyboardButton.isSelected = False
                    self.audioButton.isSelected = False
                if self.keyboardButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Keyboard"
                    self.keyboardButton.isSelected = True
                    self.pianoButton.isSelected = False
                    self.audioButton.isSelected = False
                if self.audioButton.onClick() == 1:
                    self.eventHandler.inputMethod = "Audio"
                    self.audioButton.isSelected = True
                    self.pianoButton.isSelected = False
                    self.keyboardButton.isSelected = False

                if self.vanillaButton.onClick() == 1:
                    if self.vanillaButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[0] = 1
                        self.vanillaButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[1] == 1 or self.eventHandler.gameEnharmonic[2] == 1: # Checking if at least 1 enharmonic button is selected
                        self.eventHandler.gameEnharmonic[0] = 0
                        self.vanillaButton.isSelected = False
                if self.sharpsButton.onClick() == 1:
                    if self.sharpsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[1] = 1
                        self.sharpsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[2] == 1:
                        self.eventHandler.gameEnharmonic[1] = 0
                        self.sharpsButton.isSelected = False
                if self.flatsButton.onClick() == 1:
                    if self.flatsButton.isSelected == False:
                        self.eventHandler.gameEnharmonic[2] = 1
                        self.flatsButton.isSelected = True
                    elif self.eventHandler.gameEnharmonic[0] == 1 or self.eventHandler.gameEnharmonic[1] == 1:
                        self.eventHandler.gameEnharmonic[2] = 0
                        self.flatsButton.isSelected = False
            self.draw()
        if self.eventHandler.inWhatButton == True:
            self.draw()
            if self.eventHandler.whatButtonStage == 0:
                text = ["Here you can pick your method of input"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.keyboardButton.topLeft[0], self.keyboardButton.topLeft[1], self.pianoButton.botRight[0] - self.keyboardButton.topLeft[0], self.pianoButton.botRight[1] - self.pianoButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 1:
                text = ["You can use a computer keyboard", "to practise the names of the notes"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.keyboardButton.topLeft[0], self.keyboardButton.topLeft[1], self.keyboardButton.botRight[0] - self.keyboardButton.topLeft[0], self.keyboardButton.botRight[1] - self.keyboardButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 2:
                text = ["You can connect a MIDI device (such as a piano)", "to practice matching the notes to the keys"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.pianoButton.topLeft[0], self.pianoButton.topLeft[1], self.pianoButton.botRight[0] - self.pianoButton.topLeft[0], self.pianoButton.botRight[1] - self.pianoButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 3:
                text = ["Or you can use the microphone to measure the tone", "This may be slightly less reliable"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.audioButton.topLeft[0], self.audioButton.topLeft[1], self.audioButton.botRight[0] - self.audioButton.topLeft[0], self.audioButton.botRight[1] - self.audioButton.topLeft[1], text, pos, "under")
            elif self.eventHandler.whatButtonStage == 4:
                text = ["You can also add any combination of enharmonics","If you're on keyboard, hold Shift for a Sharp and Alt for a Flat"]
                self.eventHandler.whatButtonStage += self.highLighter.highlight(self.vanillaButton.topLeft[0], self.vanillaButton.topLeft[1], self.flatsButton.botRight[0] - self.vanillaButton.topLeft[0], self.flatsButton.botRight[1] - self.vanillaButton.topLeft[1], text, pos, "above")
            if self.eventHandler.whatButtonStage == 5:
                self.eventHandler.inWhatButton = False 
                self.eventHandler.whatButtonStage = 0