import pickle
from resources.EventHandlerClass import *



# class DataIO():
#     def __init__(self):
#         self.eventHandler = EventHandler()
    
#     def write(self, data):
#         fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
#         try:
#             open(fileName, 'w').close()
#         except:
#             open(fileName, "a").close()
#         f = open(fileName, "wb")
#         pickle.dump(data, f)
#         f.close()
#         print("Data written to file")

#     def read(self):
#         fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
#         f = open(fileName, "rb")
#         data = pickle.load(f)
#         f.close()
#         print("Data read from file")
#         return data

from pathlib import Path

class DataIO():
    def __init__(self):
        self.eventHandler = EventHandler()
        self.root = Path(".")
    
    def write(self, data):
        fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
        path = self.root / "SaveData" / fileName
        try:
            open(path, 'w').close()
        except:
            open(path, "a").close()
        f = open(path, "wb")
        pickle.dump(data, f)
        f.close()
        print("Data written to file")

    def read(self):
        fileName = self.eventHandler.inputMethod + str(self.eventHandler.gameEnharmonic[0]) + str(self.eventHandler.gameEnharmonic[1]) + str(self.eventHandler.gameEnharmonic[2]) + ".p"
        path = self.root / "SaveData" / fileName
        f = open(path, "rb")
        data = pickle.load(f)
        f.close()
        print("Data read from file")
        return data