from resources.EventHandlerClass import *
from resources.HightLighterClass import *

class UI:
    def __init__(self, screen):
        self.screen = screen
        self.screensize = screen.get_size()
        self.width = self.screensize[0]
        self.height = self.screensize[1]
        self.eventHandler = EventHandler()
        self.highLighter = HighLighter(self.screen, self.eventHandler)
    
    def isActive(self): #Virtual function to be overwritten by class
        pass