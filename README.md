Most of the files in the repo are just testing things, if I haven't cleaned it up the following files are needed:
* main.py
* Marlboro.ttf
* basscleff.png
* treblecleff.png
* keydetect_total.model

main.py has dependencies on the following libraries:
* pygame
* pickle
* tensorflow
* pyaudio
* numpy
